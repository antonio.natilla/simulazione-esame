#!/bin/bash

if [ -d esercizio2_batch ]; then rm -rf esercizio2_batch; fi

cp -r esercizio2 esercizio2_batch

cd esercizio2_batch

git checkout master~3
git reset HEAD^
git add B
git commit -m "b"
git tag mountpoint
git checkout -f master
git rebase --onto mountpoint master~3 master
git tag -d mountpoint
git hist --all
