#!/bin/bash

if [ -d esercizio2_batch ]; then rm -rf esercizio2_batch; fi

cp -r esercizio2 esercizio2_batch
cd esercizio2_batch

git checkout master~3
git rm segreti.txt
git commit --amend -m 'b'
git rebase --onto HEAD master~3 master
git hist --all
ls
