# ESERCIZIO 2

- Nel repository che trovate scompattando il file es2.zip, � presente un
  repository Git in cui come terzo commit � stato tra le altre cose inserito per
  sbaglio il file segreti.txt

- Copiare tale repository in due directory distinte e, mediante due approcci
  diversi (suggerisco di pensare a un approccio batch e a uno interattivo),
  riscrivere la storia del repository in modo che l'unico cambiamento sia
  l'eliminazione del file segreti.txt dal repository

- Consegnare un file SoluzioneEs2.zip contenente:

  + le due directory con i due repo modificati
  + un file contenente la spiegazione dei comandi usati
  + le due registrazioni delle sessioni di shell con cui avete risolto
    l'esercizio
  
  
Per registrare la sessione di shell usare il comando:

asciinema rec es2Sol1

...svolgere esercizio...

dare il comando exit

non � un problema se nella registrazione ci sono pause lunghe o altre cose prima
o dopo la soluzione dell'esercizio
