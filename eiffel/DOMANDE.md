Dopo aver analizzato la classe `MY_INTERVAL`, rispondere editando il codice o
questo file a seconda del caso:

  1. modificare la post-condizione di `make`, in modo che rispecchi il commento
     che ne descrive il comportamento.

  2. la feature `is_empty` non ha nessuna post-condizione. Discuterne i 
     motivi.

  3. dopo averne definito il contratto, implementare la feature `extend_to` che
     estende l'intervallo fino a includere l'intero passato come parametro.
